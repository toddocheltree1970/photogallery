$(document).ready(function() {

    $("#settings").hide();
    $("#loading").hide();
    $('#controls').hide();
    $(".dropdown").hide();

    $('#stretch')[0].checked = false;
    $('#autostartslideshow')[0].checked = false;
    $('#addsubfolder')[0].checked = false;
    $('#fullscreen')[0].checked = false;


    var readflag;
    var files;
    var fileid;

    $('.dropdown').show();

    function hideAll() {
        $("#settings").hide();
        $('#about').hide();
        $('#privacy').hide();
    }

    $(".savesettings").click(function() {
        hideAll();
        $("#links").fadeIn();
    });

    $(".settings").click(function() {
        hideAll();
        $("#settings").fadeIn();
    });

    $('.dropbtn').click(function(event) {
        if ($('#myDropdown').hasClass('show')) {
            $("#myDropdown").removeClass('show');
        } else {
            $("#myDropdown").addClass('show');
        }
        event.stopPropagation();
    });

    $('.dropdown-content').click(function(event) {
        $("#myDropdown").removeClass('show');
        event.stopPropagation();
    });

    $(document).click(function() {
        if ($('#myDropdown').hasClass('show')) {
            $("#myDropdown").removeClass('show');
        }
    });

    window.stretchingit = !1;
    document.querySelector('#stretch').addEventListener('change', function(ev) {
        if ($("#stretch").is(':checked')) {
            window.stretchingit = !0;
        } else {
            window.stretchingit = !1;
        }

        ev.preventDefault();
    });

    window.autostartslideshow = !1;
    document.querySelector('#autostartslideshow').addEventListener('change', function(ev) {
        if ($("#autostartslideshow").is(':checked')) {
            window.autostartslideshow = !0;
        } else {
            window.autostartslideshow = !1;
        }
        ev.preventDefault();
    });

    function requestFullScreen() {
        var el = document.body;
        var requestMethod = el.requestFullScreen || el.webkitRequestFullScreen ||
            el.mozRequestFullScreen || el.msRequestFullScreen;
        if (requestMethod) {
            requestMethod.call(el);
        } else if (typeof window.ActiveXObject !== "undefined") {
            var wscript = new ActiveXObject("WScript.Shell");
            if (wscript !== null) {
                wscript.SendKeys("{F11}");
            }
        }

    }

    document.querySelector('#fullscreen').addEventListener('change', function(ev) {
        if ($("#fullscreen").is(':checked')) {
            requestFullScreen();
        } else {
            requestFullScreen();
        }
        ev.preventDefault();
    });


    window.onkeydown = window.onkeyup = function(e) {
        if (e.keyCode == 27) {
            e.preventDefault();
        }
    };

    var imagewidth;
    imagewidth = "10";
    $("#ten").css("background-color", "red");
    $("#ten").css("color", "white");
    $('.thumbsetting label').click(function() {
        if ($(this).prev().val() == 10 && $(this).prev().attr("name") == 'radio') {
            $("#ten").css("background-color", "red");
            $("#ten").css("color", "white");

            $("#twenty").css("background-color", "#f9f9f9");
            $("#twenty").css("color", "#F44336");

            $("#twentyfive").css("background-color", "#f9f9f9");
            $("#twentyfive").css("color", "#F44336");

            if ($("#links:has(img)").length > 0) {
                $('#links img').width('10%');
                done();
            }
        } else if ($(this).prev().val() == 20 && $(this).prev().attr("name") == 'radio') {
            $("#ten").css("background-color", "#f9f9f9");
            $("#ten").css("color", "#F44336");

            $("#twenty").css("background-color", "red");
            $("#twenty").css("color", "white");

            $("#twentyfive").css("background-color", "#f9f9f9");
            $("#twentyfive").css("color", "#F44336");

            if ($("#links:has(img)").length > 0) {
                $('#links img').width('20%');
                done();
            }

        } else if ($(this).prev().val() == 25 && $(this).prev().attr("name") == 'radio') {
            $("#ten").css("background-color", "#f9f9f9");
            $("#ten").css("color", "#F44336");

            $("#twenty").css("background-color", "#f9f9f9");
            $("#twenty").css("color", "#F44336");

            $("#twentyfive").css("background-color", "red");
            $("#twentyfive").css("color", "white");

            if ($("#links:has(img)").length > 0) {
                $('#links img').width('25%');
                done();
            }
        }
        imagewidth = $(this).prev().val();
    });

    $('input').keypress(function(event) {
        event.preventDefault();
    });

    window.setslideshowInterval = 5 + 'e3';
    $('#setslideshowInterval').val(5);
    $("#setslideshowInterval").on('keyup keydown blur', function(e) {
        window.setslideshowInterval = $(this).val() + 'e3';
    });


    var parsesubfolder = 0;
    document.querySelector('#addsubfolder').addEventListener('change', function(ev) {
        if ($("#addsubfolder").is(':checked')) {
            parsesubfolder = 1;
        } else {
            parsesubfolder = 0;
        }
        ev.preventDefault();
    });



    var inputfolder = document.getElementById("folderinput");
    var inputfile = document.getElementById("fileinput");


    document.getElementById('getfiles').addEventListener("click", opendialogfile, false);

    function opendialogfile() {
        $('#fileinput').trigger('click');
    }

    document.getElementById('getfolder').addEventListener("click", opendialogfolder, false);

    function opendialogfolder() {
        $('#folderinput').trigger('click');
    }


    inputfolder.addEventListener("change", function(e) {
        files = "";
        files = e.target.files;
        if (files && files.length > 0) {
            checkFileEntry(files, "folder");
        } else {
            console.log("no folders were selected");
        }
    });

    inputfile.addEventListener("change", function(e) {
        files = "";
        files = e.target.files;
        if (files && files.length > 0) {
            checkFileEntry(files, "files");
        } else {
            console.log("no files were selected");
        }
    });

    $(".about").click(function() {
        $('#about').fadeIn();
        $('#container')[0].scrollIntoView(true);
    });

    $("#about").click(function() {
        $('#about').fadeOut();
        $('#container')[0].scrollIntoView(true);
    });

    $(".privacy").click(function() {
        $('#privacy').fadeIn();
        $('#container')[0].scrollIntoView(true);
    });

    $("#privacy").click(function() {
        $('#privacy').fadeOut();
        $('#container')[0].scrollIntoView(true);
    });

    $("#closeitp").click(function() {
        $('#privacy').fadeOut();
        $('#container')[0].scrollIntoView(true);
    });

    $("#closeita").click(function() {
        $('#about').fadeOut();
        $('#container')[0].scrollIntoView(true);
    });

    function adding() {
        $("#loading").show();
        $("#links").show();
        count = 0;
        document.body.style.overflowY = 'hidden';
        document.body.style.overflowX = 'hidden';
        $('#links #loading #count').text('Loading... ');
    }

    var finalcountj;

    function checkFileEntry(fileEntries, type) {
        var inputtype = type;
        var imglist = [];

        for (var i = 0; i < fileEntries.length; i++) {
            var item = fileEntries[i];
            var webkitpath = item.webkitRelativePath;
            var subpathcount = webkitpath.split("/").length - 1;
            if (inputtype === "folder") {
                if (parsesubfolder === 0 && subpathcount === 1) {
                    if (checkFileType(item.name)) {
                        imglist.push(item);
                    }
                } else if (parsesubfolder === 1) {
                    if (checkFileType(item.name)) {
                        imglist.push(item);
                    }
                }
            } else if (inputtype === "files") {
                if (checkFileType(item.name)) {
                    imglist.push(item);
                }
            }
        }

        if (imglist.length > 0) {
            $('#footer-left').hide();
            $('#footer-right').hide();
            $('#branding').hide();
            adding();
            finalcountj = imglist.length;
            for (var j = 0; j < imglist.length; j++) {
                addImageFile(imglist, j);
            }
        } else {
            setTimeout(function() {}, 1000);
        }
        if (imglist.length === 0) {
            $("#loading").hide();
        }
    }

    function checkFileType(FileName) {
        var type = "";
        var fname = FileName.replace(/(jpg-large|jpg:large)/i, "jpg");
        if (/\.(jpe?g)$/i.test(fname)) {
            type = "jpg";
        } else if (/\.(png)$/i.test(fname)) {
            type = "png";
        } else if (/\.(gif)$/i.test(fname)) {
            type = "gif";
        } else if (/\.(bmp)$/i.test(fname)) {
            type = "bmp";
        } else if (/\.(webp)$/i.test(fname)) {
            type = "webp";
        } else if (/\.(jpeg)$/i.test(fname)) {
            type = "jpeg";
        } else if (/\.(svg)$/i.test(fname)) {
            type = "svg";
        } else if (/\.(xbm)$/i.test(fname)) {
            type = "xbm";
        }
        return type;
    }
    var $container;

    function done() {
        if ($container) {
            $container.masonry('destroy');
        }
        $container = $('#links');
        var masonryOptions = {
            itemSelector: 'img',
            columnWidth: '.links',
            percentPosition: true
        };
        $container.masonry(masonryOptions);
        $("#loading").hide();
        document.body.style.overflowY = 'scroll';
        document.body.style.overflowX = 'hidden';
    }


    var count = 0;

    function addImageFile(imglist, idx) {
        var obj = {};
        obj.name = imglist[idx].name;
        obj.url = window.URL.createObjectURL(imglist[idx]);
        setTimeout(function() {
            $("<a href=" + obj.url + " title=" + obj.name + " data-gallery=''><img style='width: " + imagewidth + "%;' class='links' src=" + obj.url + "></a>").appendTo("#links").hide().fadeIn('fast').imagesLoaded()
                .done(function(instance) {
                    count = count + 1;
                    $('#links #loading #count').text('Loading ' + count + ' of ' + finalcountj).hide().show();
                    if (count === finalcountj) {
                        done();
                    }
                });
        }, 1000);
    }

});
